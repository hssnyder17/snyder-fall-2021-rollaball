using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private void OnTriggerEnter(Collider Other)
    {
        if (Other.gameObject.CompareTag("Player"))
        {
            PlayerController playerController = Other.gameObject.GetComponent<PlayerController>();

            if(playerController != null)
            {
                Vector3 newSpawnPoint = transform.position;

                playerController.SetNewRespawn(newSpawnPoint);
            }
        }
    }

}
